Pod::Spec.new do |spec|
  spec.name         = 'GoogleAnalytics-iOS-SDK'
  spec.version      = '3.13'
  spec.license      = { :type => 'Copyright', :text => 'Copyright 2009 - 2015 Google, Inc. All rights reserved.'}
  spec.summary      = 'The Google Analytics SDK for iOS makes it easy for native iOS developers to collect user engagement data form their applications. Developers can then use the Google Analytics reports to measure:\n\n* The number of active users are using their applications.\n* From where in the world the application is being used.\n* Adoption and usage of specific features.\n* In-app purchases and transactions.\n* And many other useful metrics...\n'
  spec.homepage     = 'https://developers.google.com/analytics/devguides/collection/ios/'
  spec.author       = 'Google Inc.'
  spec.source       = { :http => 'https://dl.google.com/googleanalyticsservices/GoogleAnalyticsServicesiOS_3.13.zip', :flatten => true }
  spec.platform = :ios, "5.0"
  spec.requires_arc = true
  spec.deprecated_in_favor_of = 'GoogleAnalytics'

  spec.default_subspecs = 'Core'
  spec.subspec 'Core' do |ss|
    ss.name = 'Core'
    ss.source_files = 'GoogleAnalytics/Library/*.h', 'GoogleTagManager/Library/*.h'
    ss.preserve_paths = 'libGoogleAnalyticsServices.a'
    ss.frameworks = 'Foundation', 'UIKit', 'CFNetwork', 'CoreData', 'SystemConfiguration'
    ss.libraries = 'GoogleAnalyticsServices', 'z', 'sqlite3'
    ss.xcconfig = {:LIBRARY_SEARCH_PATHS => '"~/Documents/Libs/ObjC/GoogleAnalyticsServicesiOS_3.13"', :HEADER_SEARCH_PATHS => '"$(SDKROOT)/usr/include/libz"'}
  end
  spec.subspec 'IDFA' do |ss|
    ss.name = 'IDFA'
    ss.preserve_paths = 'libAdIdAccess.a'
    ss.weak_frameworks = 'AdSupport'
    ss.libraries = 'AdIdAccess'
    ss.dependencies = 'GoogleAnalytics-iOS-SDK/Core'
  end
end